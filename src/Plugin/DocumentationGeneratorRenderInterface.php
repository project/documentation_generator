<?php

namespace Drupal\documentation_generator\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Documentation Generator Render plugins.
 */
interface DocumentationGeneratorRenderInterface extends PluginInspectionInterface {

  /**
   * Provide the extension of the generated file.
   *
   * @return string
   *   The extension.
   */
  public function getExtension();

  /**
   * Provide the output of the documentation.
   *
   * @param string $fileName
   *   The name of the documentation.
   * @param string $path
   *   The patch where to store the generated file.
   * @param string $title
   *   The document title.
   * @param array $groups
   *   The elements to render.
   *
   * @return bool
   *   Either or not the generation was successful.
   */
  public function render($fileName, $path, $title, array $groups);

}
