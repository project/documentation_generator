<?php

namespace Drupal\documentation_generator\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ExtensionList;

/**
 * Base class for Documentation Generator Chapter plugins.
 */
abstract class DocumentationGeneratorChapterBase extends PluginBase implements DocumentationGeneratorChapterInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Module Extension List .
   *
   * @var \Drupal\Core\Extension\ExtensionList
   */
  protected $extensionList;

  /**
   * The constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin identifier.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityfieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Extension\ExtensionList $extensionList
   *   The module extension list.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager,
    EntityfieldManagerInterface $entityFieldManager,
    ModuleHandlerInterface $moduleHandler,
    ConfigFactoryInterface $configFactory,
    ExtensionList $extensionList,
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->moduleHandler = $moduleHandler;
    $this->configFactory = $configFactory;
    $this->extensionList = $extensionList;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('module_handler'),
      $container->get('config.factory'),
      $container->get('extension.list.module')
    );
  }

  /**
   * Determine if the plugin is available.
   *
   * @return bool
   *   Either or not the plugin is available.
   */
  public function available() {
    $dependencies = $this->moduleDependencies();

    foreach ($dependencies as $dependency) {
      if (!$this->moduleHandler->moduleExists($dependency)) {
        return FALSE;
      }
    }

    $enabledPlugins = $this->configFactory->get('documentation_generator.enabled_plugins')->get('plugins');
    if (!in_array($this->pluginId, $enabledPlugins)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Remove disabled elements.
   *
   * @param array $elements
   *   The elements to filter.
   */
  public function removeDisabledElements(array &$elements) {
    $disabledElements = $this->configFactory->get('documentation_generator.disabled_elements')->get('elements');

    foreach ($elements as $key => $menu) {
      if (in_array($this->pluginId . '_' . $key, $disabledElements)) {
        unset($elements[$key]);
      }
    }
  }

}
