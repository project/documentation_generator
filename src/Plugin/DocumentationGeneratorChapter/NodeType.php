<?php

namespace Drupal\documentation_generator\Plugin\DocumentationGeneratorChapter;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Url;
use Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterBase;

/**
 * Node Type Documentation Generator chapter.
 *
 * Implements Documentation Generator Chapter plugin for Node Type.
 *
 * @DocumentationGeneratorChapter(
 *   id = "node_type",
 *   label = @Translation("Content Type")
 * )
 */
class NodeType extends DocumentationGeneratorChapterBase {

  /**
   * {@inheritdoc}
   */
  public function moduleDependencies() {
    return [
      'node',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function pluginElements() {
    $nodeTypes = $this->entityTypeManager
      ->getStorage('node_type')
      ->loadMultiple();

    return $nodeTypes;
  }

  /**
   * {@inheritdoc}
   */
  public function elements() {
    $elements = [];

    $elements[] = [
      'type' => 'title',
      'level' => 1,
      'value' => $this->t('Content Types')->render(),
    ];

    $url = Url::fromUserInput('/admin/content')->setAbsolute()->toString();
    $elements[] = [
      'type' => 'paragraph',
      'level' => 2,
      'value' => $this->t('This section provides information about Node Types : @parameter')->render(),
      'parameters' => [
        0 => [
          'type' => 'link',
          'text' => $url,
          'src' => $url,
        ],
      ],
    ];

    $nodeTypes = $this->pluginElements();
    $this->removeDisabledElements($nodeTypes);

    foreach ($nodeTypes as $nodeType) {
      $overviewUrl = $url = Url::fromUserInput('/admin/content', [
        'query' => [
          'type' => $nodeType->id(),
        ],
      ])->setAbsolute()->toString();

      $elements[] = [
        'type' => 'title',
        'level' => 2,
        'value' => $nodeType->label(),
      ];

      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $nodeType->getDescription(),
      ];

      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $this->t('You can find all @type at : @parameter', [
          '@type' => $nodeType->label(),
        ])->render(),
        'parameters' => [
          0 => [
            'type' => 'link',
            'text' => $overviewUrl,
            'src' => $overviewUrl,
          ],
        ],
      ];

      $url = Url::fromUserInput('/node/add/' . $nodeType->id())->setAbsolute()->toString();
      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $this->t('You can add @type at : @parameter', [
          '@type' => $nodeType->label(),
        ])->render(),
        'parameters' => [
          0 => [
            'type' => 'link',
            'text' => $url,
            'src' => $url,
          ],
        ],
      ];

      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $this->t('To edit a @type, go to @parameter and click on the "Edit" button on the right ("Action" column).', [
          '@type' => $nodeType->label(),
        ])->render(),
        'parameters' => [
          0 => [
            'type' => 'link',
            'text' => $overviewUrl,
            'src' => $overviewUrl,
          ],
        ],
      ];

      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $this->t('To delete a @type, go to @parameter and click on the arrow on the right of the "Edit" button ("Action" column), then select "Delete".', [
          '@type' => $nodeType->label(),
        ])->render(),
        'parameters' => [
          0 => [
            'type' => 'link',
            'text' => $overviewUrl,
            'src' => $overviewUrl,
          ],
        ],
      ];

      $fields = $this->entityFieldManager->getFieldDefinitions('node', $nodeType->id());
      $customFields = [];
      foreach ($fields as $field) {
        if (!($field instanceof BaseFieldDefinition)) {
          $customFields[] = $field;
        }
      }
      if (!empty($customFields)) {
        $elements[] = [
          'type' => 'title',
          'level' => 3,
          'value' => $this->t('Available fields')->render(),
        ];
        foreach ($customFields as $field) {
          $description = $field->get('description') ?: 'No Description';
          $elements[] = [
            'type' => 'paragraph',
            'level' => 4,
            'value' => $field->getLabel() . ' : ' . $description,
          ];
        }
      }
    }

    return $elements;
  }

}
