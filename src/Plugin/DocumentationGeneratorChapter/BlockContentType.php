<?php

namespace Drupal\documentation_generator\Plugin\DocumentationGeneratorChapter;

use Drupal\Core\Url;
use Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterBase;

/**
 * Block Content Type Documentation Generator chapter.
 *
 * Implements Documentation Generator Chapter plugin for Block Content Type.
 *
 * @DocumentationGeneratorChapter(
 *   id = "block_content_type",
 *   label = @Translation("Block Content Type")
 * )
 */
class BlockContentType extends DocumentationGeneratorChapterBase {

  /**
   * {@inheritdoc}
   */
  public function moduleDependencies() {
    return [
      'block_content',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function pluginElements() {
    $blocks = $this->entityTypeManager
      ->getStorage('block_content_type')
      ->loadMultiple();

    return $blocks;
  }

  /**
   * {@inheritdoc}
   */
  public function elements() {
    $elements = [];

    $elements[] = [
      'type' => 'title',
      'level' => 1,
      'value' => $this->t('Block Content Types')->render(),
    ];

    $elements[] = [
      'type' => 'paragraph',
      'level' => 2,
      'value' => $this->t('This section provides information about blocks content types : @parameter')->render(),
      'parameters' => [
        0 => [
          'type' => 'link',
          'text' => $this->t('See all blocks')->render(),
          'src' => Url::fromUserInput('/admin/structure/block/block-content')->setAbsolute()->toString(),
        ],
      ],
    ];

    $blocks = $this->pluginElements();
    $this->removeDisabledElements($blocks);

    foreach ($blocks as $block) {
      $elements[] = [
        'type' => 'title',
        'level' => 2,
        'value' => $block->label(),
      ];

      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $block->getDescription(),
      ];
    }

    return $elements;
  }

}
