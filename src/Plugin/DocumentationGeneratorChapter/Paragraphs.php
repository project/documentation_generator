<?php

namespace Drupal\documentation_generator\Plugin\DocumentationGeneratorChapter;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Url;
use Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterBase;

/**
 * Paragraph Types Documentation Generator chapter.
 *
 * Implements Documentation Generator Chapter plugin for Paragraph Types.
 *
 * @DocumentationGeneratorChapter(
 *   id = "paragraph",
 *   label = @Translation("Paragraph Types")
 * )
 */
class Paragraphs extends DocumentationGeneratorChapterBase {

  /**
   * {@inheritdoc}
   */
  public function moduleDependencies() {
    return [
      'paragraphs',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function pluginElements() {
    $paragraphTypes = $this->entityTypeManager
      ->getStorage('paragraphs_type')
      ->loadMultiple();

    return $paragraphTypes;
  }

  /**
   * {@inheritdoc}
   */
  public function elements() {
    $elements = [];

    $elements[] = [
      'type' => 'title',
      'level' => 1,
      'value' => $this->t('Paragraph Types')->render(),
    ];

    $url = Url::fromUserInput('/admin/structure/paragraphs_type')->setAbsolute()->toString();
    $elements[] = [
      'type' => 'paragraph',
      'level' => 2,
      'value' => $this->t('This section provides information about Paragraph Types : @parameter')->render(),
      'parameters' => [
        0 => [
          'type' => 'link',
          'text' => $url,
          'src' => $url,
        ],
      ],
    ];

    $paragraphTypes = $this->pluginElements();
    $this->removeDisabledElements($paragraphTypes);

    foreach ($paragraphTypes as $paragrapgh) {
      $elements[] = [
        'type' => 'title',
        'level' => 2,
        'value' => $paragrapgh->label(),
      ];
      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $paragrapgh->get('description') ?: 'No Description',
      ];

      $fields = $this->entityFieldManager->getFieldDefinitions('paragraph', $paragrapgh->id());
      $customFields = [];
      foreach ($fields as $field) {
        if (!($field instanceof BaseFieldDefinition)) {
          $customFields[] = $field;
        }
      }
      if (!empty($customFields)) {
        $elements[] = [
          'type' => 'title',
          'level' => 3,
          'value' => $this->t('Available fields')->render(),
        ];
        foreach ($customFields as $field) {
          $description = $field->get('description') ?: 'No Description';
          $elements[] = [
            'type' => 'paragraph',
            'level' => 4,
            'value' => $field->getLabel() . ' : ' . $description,
          ];
        }
      }
    }

    return $elements;
  }

}
