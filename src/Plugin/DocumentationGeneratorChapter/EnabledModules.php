<?php

namespace Drupal\documentation_generator\Plugin\DocumentationGeneratorChapter;

use Drupal\Core\Url;
use Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterBase;

/**
 * Enabled Modules Documentation Generator chapter.
 *
 * Implements Documentation Generator Chapter plugin for Modules.
 *
 * @DocumentationGeneratorChapter(
 *   id = "enabled_modules",
 *   label = @Translation("Modules")
 * )
 */
class EnabledModules extends DocumentationGeneratorChapterBase {

  /**
   * {@inheritdoc}
   */
  public function moduleDependencies() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function pluginElements() {
    $enabledModules = $this->moduleHandler
      ->getModuleList();

    return $enabledModules;
  }

  /**
   * {@inheritdoc}
   */
  public function elements() {
    $elements = [];

    $elements[] = [
      'type' => 'title',
      'level' => 1,
      'value' => $this->t('Modules')->render(),
    ];

    $url = Url::fromUserInput('/admin/modules')->setAbsolute()->toString();
    $elements[] = [
      'type' => 'paragraph',
      'level' => 2,
      'value' => $this->t('This section provides information about enabled modules : @parameter')->render(),
      'parameters' => [
        0 => [
          'type' => 'link',
          'text' => $url,
          'src' => $url,
        ],
      ],
    ];

    $enabledModules = $this->pluginElements();
    $this->removeDisabledElements($enabledModules);
    $info = $this->extensionList->getAllInstalledInfo();

    foreach (array_keys($enabledModules) as $module) {
      $helpUrl = $url = Url::fromUserInput('/admin/help/' . $module)->setAbsolute()->toString();

      $description = $info[$module]['description'];

      $elements[] = [
        'type' => 'title',
        'level' => 2,
        'value' => $this->moduleHandler->getName($module),
      ];

      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $description,
      ];

      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $this->t(
            'More about @type at : @parameter', [
              '@type' => $this->moduleHandler->getName($module),
            ]
        )->render(),
        'parameters' => [
          0 => [
            'type' => 'link',
            'text' => $helpUrl,
            'src' => $helpUrl,
          ],
        ],
      ];
    }
    return $elements;
  }

}
