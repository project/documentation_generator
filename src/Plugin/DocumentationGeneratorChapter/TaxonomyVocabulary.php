<?php

namespace Drupal\documentation_generator\Plugin\DocumentationGeneratorChapter;

use Drupal\Core\Url;
use Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterBase;

/**
 * Taxonomy Vocabulary Documentation Generator chapter.
 *
 * Implements Documentation Generator Chapter plugin for Taxonomy Vocabulary .
 *
 * @DocumentationGeneratorChapter(
 *   id = "taxonomy_vocabulary",
 *   label = @Translation("Taxonomy Vocabulary ")
 * )
 */
class TaxonomyVocabulary extends DocumentationGeneratorChapterBase {

  /**
   * {@inheritdoc}
   */
  public function moduleDependencies() {
    return [
      'taxonomy',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function pluginElements() {
    $vocabularies = $this->entityTypeManager
      ->getStorage('taxonomy_vocabulary')
      ->loadMultiple();

    return $vocabularies;
  }

  /**
   * {@inheritdoc}
   */
  public function elements() {
    $elements = [];

    $elements[] = [
      'type' => 'title',
      'level' => 1,
      'value' => $this->t('Taxonomy Vocabularies')->render(),
    ];

    $url = Url::fromUserInput('/admin/structure/taxonomy')->setAbsolute()->toString();
    $elements[] = [
      'type' => 'paragraph',
      'level' => 2,
      'value' => $this->t('This section provides information about Taxonomy Vocabularies : @parameter')->render(),
      'parameters' => [
        0 => [
          'type' => 'link',
          'text' => $url,
          'src' => $url,
        ],
      ],
    ];

    $vocabularies = $this->pluginElements();
    $this->removeDisabledElements($vocabularies);

    foreach ($vocabularies as $vocabulary) {
      $overviewUrl = Url::fromUserInput('/admin/structure/taxonomy/manage/' . $vocabulary->id() . '/overview')->setAbsolute()->toString();

      $elements[] = [
        'type' => 'title',
        'level' => 2,
        'value' => $vocabulary->label(),
      ];

      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $vocabulary->getDescription(),
      ];

      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $this->t('You can find and reorganize all @type at : @parameter', [
          '@type' => $vocabulary->label(),
        ])->render(),
        'parameters' => [
          0 => [
            'type' => 'link',
            'text' => $overviewUrl,
            'src' => $overviewUrl,
          ],
        ],
      ];

      $url = Url::fromUserInput('/admin/structure/taxonomy/manage/' . $vocabulary->id() . '/add')->setAbsolute()->toString();
      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $this->t('You can add @type at : @parameter', [
          '@type' => $vocabulary->label(),
        ])->render(),
        'parameters' => [
          0 => [
            'type' => 'link',
            'text' => $url,
            'src' => $url,
          ],
        ],
      ];

      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $this->t('To edit a @type, go to @parameter and click on the "Edit" button on the right ("Action" column).', [
          '@type' => $vocabulary->label(),
        ])->render(),
        'parameters' => [
          0 => [
            'type' => 'link',
            'text' => $overviewUrl,
            'src' => $overviewUrl,
          ],
        ],
      ];

      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $this->t('To delete a @type, go to @parameter and click on the arrow on the right of the "Edit" button ("Action" column), then select "Delete".', [
          '@type' => $vocabulary->label(),
        ])->render(),
        'parameters' => [
          0 => [
            'type' => 'link',
            'text' => $overviewUrl,
            'src' => $overviewUrl,
          ],
        ],
      ];
    }

    return $elements;
  }

}
