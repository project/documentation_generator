<?php

namespace Drupal\documentation_generator\Plugin\DocumentationGeneratorChapter;

use Drupal\Core\Url;
use Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterBase;

/**
 * User Role Documentation Generator chapter.
 *
 * Implements Documentation Generator Chapter plugin for User Role.
 *
 * @DocumentationGeneratorChapter(
 *   id = "user_role",
 *   label = @Translation("User Role")
 * )
 */
class UserRole extends DocumentationGeneratorChapterBase {

  /**
   * {@inheritdoc}
   */
  public function moduleDependencies() {
    return [
      'user',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function pluginElements() {
    $userRoles = $this->entityTypeManager
      ->getStorage('user_role')
      ->loadMultiple();

    return $userRoles;
  }

  /**
   * {@inheritdoc}
   */
  public function elements() {
    $elements = [];

    $elements[] = [
      'type' => 'title',
      'level' => 1,
      'value' => $this->t('User Roles')->render(),
    ];

    $elements[] = [
      'type' => 'paragraph',
      'level' => 2,
      'value' => $this->t('This section provides information about User Roles : @parameter')->render(),
      'parameters' => [
        0 => [
          'type' => 'link',
          'text' => $this->t('See all roles')->render(),
          'src' => Url::fromUserInput('/admin/people/roles')->setAbsolute()->toString(),
        ],
      ],
    ];

    $userRoles = $this->pluginElements();
    $this->removeDisabledElements($userRoles);

    foreach ($userRoles as $userRole) {
      $elements[] = [
        'type' => 'title',
        'level' => 2,
        'value' => $userRole->label(),
      ];
    }

    return $elements;
  }

}
