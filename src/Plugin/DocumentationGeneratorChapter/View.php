<?php

namespace Drupal\documentation_generator\Plugin\DocumentationGeneratorChapter;

use Drupal\Core\Url;
use Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterBase;

/**
 * View Documentation Generator chapter.
 *
 * Implements Documentation Generator Chapter plugin for View.
 *
 * @DocumentationGeneratorChapter(
 *   id = "view",
 *   label = @Translation("View")
 * )
 */
class View extends DocumentationGeneratorChapterBase {

  /**
   * {@inheritdoc}
   */
  public function moduleDependencies() {
    return [
      'views',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function pluginElements() {
    $views = $this->entityTypeManager
      ->getStorage('view')
      ->loadMultiple();

    return $views;
  }

  /**
   * {@inheritdoc}
   */
  public function elements() {
    $elements = [];

    $elements[] = [
      'type' => 'title',
      'level' => 1,
      'value' => $this->t('Views')->render(),
    ];

    $elements[] = [
      'type' => 'paragraph',
      'level' => 2,
      'value' => $this->t('This section provides information about Views : @parameter')->render(),
      'parameters' => [
        0 => [
          'type' => 'link',
          'text' => $this->t('See all views')->render(),
          'src' => Url::fromUserInput('/admin/structure/views')->setAbsolute()->toString(),
        ],
      ],
    ];

    $views = $this->pluginElements();
    $this->removeDisabledElements($views);

    foreach ($views as $view) {
      $elements[] = [
        'type' => 'title',
        'level' => 2,
        'value' => $view->label(),
      ];
      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $view->get('description') ?: 'No Description',
      ];
    }

    return $elements;
  }

}
