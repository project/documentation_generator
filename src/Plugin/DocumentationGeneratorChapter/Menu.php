<?php

namespace Drupal\documentation_generator\Plugin\DocumentationGeneratorChapter;

use Drupal\Core\Url;
use Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterBase;

/**
 * Menu Documentation Generator chapter.
 *
 * Implements Documentation Generator Chapter plugin for Menu.
 *
 * @DocumentationGeneratorChapter(
 *   id = "menu",
 *   label = @Translation("Menu")
 * )
 */
class Menu extends DocumentationGeneratorChapterBase {

  /**
   * {@inheritdoc}
   */
  public function moduleDependencies() {
    return [
      'menu_ui',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function pluginElements() {
    $menus = $this->entityTypeManager
      ->getStorage('menu')
      ->loadMultiple();

    return $menus;
  }

  /**
   * {@inheritdoc}
   */
  public function elements() {
    $elements = [];

    $elements[] = [
      'type' => 'title',
      'level' => 1,
      'value' => $this->t('Menus')->render(),
    ];

    $url = Url::fromUserInput('/admin/structure/menu')->setAbsolute()->toString();
    $elements[] = [
      'type' => 'paragraph',
      'level' => 2,
      'value' => $this->t('This section provides information about menus : @parameter')->render(),
      'parameters' => [
        0 => [
          'type' => 'link',
          'text' => $url,
          'src' => $url,
        ],
      ],
    ];

    $menus = $this->pluginElements();
    $this->removeDisabledElements($menus);

    $defaultThemeName = $this->configFactory->get('system.theme')->get('default');

    $blockMenusPluginsName = [];
    foreach ($menus as $key => $menu) {
      $blockMenusPluginsName[] = 'system_menu_block:' . $key;
    }
    $blocks = $this->entityTypeManager
      ->getStorage('block')
      ->loadByProperties([
        'theme' => $defaultThemeName,
        'plugin' => $blockMenusPluginsName,
      ]);
    $blockMenus = [];
    foreach ($blocks as $block) {
      $blockMenus[$block->getPluginId()] = $block;
    }

    $regions = system_region_list($defaultThemeName);

    foreach ($menus as $menu) {
      $overviewUrl = url::fromUserInput('/admin/structure/menu/manage/' . $menu->id())->setAbsolute()->toString();

      $elements[] = [
        'type' => 'title',
        'level' => 2,
        'value' => $menu->label(),
      ];

      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $menu->getDescription(),
      ];

      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $this->t('You can find all links at : @parameter')->render(),
        'parameters' => [
          0 => [
            'type' => 'link',
            'text' => $overviewUrl,
            'src' => $overviewUrl,
          ],
        ],
      ];

      $url = Url::fromUserInput('/admin/structure/menu/manage/' . $menu->id() . '/add')->setAbsolute()->toString();
      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $this->t('You can add a link at : @parameter')->render(),
        'parameters' => [
          0 => [
            'type' => 'link',
            'text' => $url,
            'src' => $url,
          ],
        ],
      ];

      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $this->t('To edit a link, go to @parameter and click on the "Edit" button on the right ("Action" column).')->render(),
        'parameters' => [
          0 => [
            'type' => 'link',
            'text' => $overviewUrl,
            'src' => $overviewUrl,
          ],
        ],
      ];

      $elements[] = [
        'type' => 'paragraph',
        'level' => 3,
        'value' => $this->t('To delete a link, go to @parameter and click on the arrow on the right of the "Edit" button ("Action" column), then select "Delete".')->render(),
        'parameters' => [
          0 => [
            'type' => 'link',
            'text' => $overviewUrl,
            'src' => $overviewUrl,
          ],
        ],
      ];

      if (!empty($blockMenus['system_menu_block:' . $menu->id()])
        && $menuBlockSettings = $blockMenus['system_menu_block:' . $menu->id()]
      ) {
        $items = [];

        $region = $regions[$menuBlockSettings->getRegion()]->render();
        $items[] = $this->t('In region "@region"', [
          '@region' => $region,
        ])->render();

        if ($visibility = $menuBlockSettings->getVisibility()) {
          if (!empty($visibility['node_type']['bundles'])) {
            if (!$visibility['node_type']['negate']) {
              $items[] = $this->t('On node pages "@types"', [
                '@types' => implode(', ', $visibility['node_type']['bundles']),
              ])->render();
            }
            else {
              $items[] = $this->t('On all node pages except "@types"', [
                '@types' => implode(', ', $visibility['node_type']['bundles']),
              ])->render();
            }
          }

          if (!empty($visibility['request_path']['pages'])) {
            $urls = implode(', ', explode("\r\n", $visibility['request_path']['pages']));
            $urls = str_replace('<front>', $this->t('Homepage')->render(), $urls);

            if (!$visibility['request_path']['negate']) {
              $items[] = $this->t('On urls "@urls"', [
                '@urls' => $urls,
              ])->render();
            }
            else {
              $items[] = $this->t('On all urls except "@urls"', [
                '@urls' => $urls,
              ])->render();
            }
          }

          if (!empty($visibility['user_role']['roles'])) {
            if (!$visibility['request_path']['negate']) {
              $items[] = $this->t('For users with roles "@roles"', [
                '@roles' => implode(', ', $visibility['user_role']['roles']),
              ])->render();
            }
            else {
              $items[] = $this->t('For users without roles "@roles"', [
                '@roles' => implode(', ', $visibility['user_role']['roles']),
              ])->render();
            }
          }
        }
        else {
          $items[] = $this->t('On all pages of the site')->render();
        }

        $elements[] = [
          'type' => 'paragraph',
          'level' => 3,
          'value' => $this->t('This menu is displayed : @parameter')->render(),
          'parameters' => [
            0 => [
              'type' => 'list',
              'items' => $items,
            ],
          ],
        ];
      }
      else {
        $elements[] = [
          'type' => 'paragraph',
          'level' => 3,
          'value' => $this->t('This menu is not displayed on the site.')->render(),
        ];
      }
    }

    return $elements;
  }

}
