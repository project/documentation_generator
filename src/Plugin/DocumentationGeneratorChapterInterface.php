<?php

namespace Drupal\documentation_generator\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Documentation Generator Chapter plugins.
 */
interface DocumentationGeneratorChapterInterface extends PluginInspectionInterface {

  /**
   * Provide the dependencies on modules.
   *
   * @return array
   *   An array of module machine name.
   */
  public function moduleDependencies();

  /**
   * Provide the element the plugin will list in the documentation.
   *
   * @return array
   *   An array of mixed.
   */
  public function pluginElements();

  /**
   * Provide elements of the chapter.
   *
   * @return array
   *   An array of arrays.
   */
  public function elements();

}
