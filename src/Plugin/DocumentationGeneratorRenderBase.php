<?php

namespace Drupal\documentation_generator\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Documentation Generator Render plugins.
 */
abstract class DocumentationGeneratorRenderBase extends PluginBase implements DocumentationGeneratorRenderInterface {

}
