<?php

namespace Drupal\documentation_generator\Plugin\DocumentationGeneratorRender;

use Drupal\Component\Utility\Html;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\documentation_generator\Plugin\DocumentationGeneratorRenderBase;
use PhpOffice\PhpWord\Exception\Exception;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Word Documentation Generator render.
 *
 * Implements Documentation Generator Render plugin for Word.
 *
 * @DocumentationGeneratorRender(
 *   id = "word",
 *   label = @Translation("Word")
 * )
 */
class Word extends DocumentationGeneratorRenderBase implements ContainerFactoryPluginInterface {

  /**
   * The php word.
   *
   * @var \PhpOffice\PhpWord\PhpWord
   */
  protected $phpWord;

  /**
   * Constructs a new Word instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \PhpOffice\PhpWord\PhpWord $phpWord
   *   The php word.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PhpWord $phpWord) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->phpWord = $phpWord;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('documentation_generator.php_word')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getExtension() {
    return 'docx';
  }

  /**
   * {@inheritdoc}
   */
  public function render($fileName, $path, $title, array $groups) {
    $this->phpWord->addTitleStyle(1, [
      'color' => '2e74b4',
      'name' => 'Calibri Light',
      'size' => 16,
    ], []);
    $this->phpWord->addTitleStyle(2, [
      'color' => '2e74b4',
      'name' => 'Calibri Light',
      'size' => 13,
    ], []);
    $this->phpWord->addTitleStyle(3, [
      'color' => '1f4d78',
      'name' => 'Calibri Light',
      'size' => 12,
    ], []);
    $this->phpWord->addFontStyle('Link', [
      'color' => '0000FF',
      'underline' => 'single',
    ]);

    $titleSection = $this->phpWord->addSection();
    $titleSection->addText($title, [
      'color' => '000000',
      'name' => 'Calibri Light',
      'size' => 28,
    ], []);

    $tableOfContentsSection = $this->phpWord->addSection();
    $tableOfContentsSection->addTOC(NULL, NULL, 1, 1);

    foreach ($groups as $group) {
      $section = $this->phpWord->addSection();

      foreach ($group as $element) {
        if (!empty($element['value'])) {
          if ($element['type'] == 'title') {
            $section->addTitle($element['value'], $element['level']);
          }
          elseif ($element['type'] == 'paragraph') {
            if (!empty($element['parameters'])) {
              $element['value'] = explode('@parameter', $element['value']);

              foreach ($element['value'] as $k => $v) {
                if (!empty($v)) {
                  $v = Html::escape($v);
                  $section->addText($v);
                }

                if (!empty($element['parameters'][$k])) {
                  switch ($element['parameters'][$k]['type']) {
                    case 'link':
                      $element['parameters'][$k]['text'] = Html::escape($element['parameters'][$k]['text']);
                      $section->addLink($element['parameters'][$k]['src'], $element['parameters'][$k]['text'], 'Link');
                      break;

                    case 'list':
                      foreach ($element['parameters'][$k]['items'] as $item) {
                        $section->addListItem($item);
                      }
                      break;
                  }
                }
              }
            }
            else {
              $element['value'] = Html::escape($element['value']);
              $section->addText($element['value']);
            }
          }
        }
      }
    }

    $success = TRUE;

    try {
      $objWriter = IOFactory::createWriter($this->phpWord, 'Word2007');
      $objWriter->save($path . '/' . $fileName);
    }
    catch (Exception $e) {
      $success = FALSE;
    }

    return $success;
  }

}
