<?php

namespace Drupal\documentation_generator\Plugin\DocumentationGeneratorRender;

use Dompdf\Dompdf;
use Drupal\Core\File\Exception\FileException;
use Drupal\file\FileRepositoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\documentation_generator\Plugin\DocumentationGeneratorRenderBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * PDF Documentation Generator render.
 *
 * Implements Documentation Generator Render plugin for PDF.
 *
 * @DocumentationGeneratorRender(
 *   id = "pdf",
 *   label = @Translation("PDF")
 * )
 */
class PDF extends DocumentationGeneratorRenderBase implements ContainerFactoryPluginInterface {

  /**
   * The dompdf.
   *
   * @var \Dompdf\Dompdf
   */
  protected $dompdf;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The file repository.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * Constructs a new PDF instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Dompdf\Dompdf $dompdf
   *   The dompdf.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Dompdf $dompdf, RendererInterface $renderer, FileRepositoryInterface $file_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->dompdf = $dompdf;
    $this->renderer = $renderer;
    $this->fileRepository = $file_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('documentation_generator.dompdf'),
      $container->get('renderer'),
      $container->get('file.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getExtension() {
    return 'pdf';
  }

  /**
   * {@inheritdoc}
   */
  public function render($fileName, $path, $title, array $groups) {
    $build = [
      '#theme' => 'documentation',
      '#title' => $title,
      '#groups' => $groups,
    ];

    $this->dompdf->loadHtml($this->renderer->render($build));
    $this->dompdf->render();
    $output = $this->dompdf->output();

    $success = TRUE;
    try {
      $this->fileRepository->writeData($output, 'private://' . $fileName);
    }
    catch (FileException $e) {
      $success = FALSE;
    }

    return $success;
  }

}
