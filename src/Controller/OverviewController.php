<?php

namespace Drupal\documentation_generator\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Markup;
use Drupal\Core\Render\RendererInterface;
use Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides documentation overview controller.
 */
class OverviewController extends ControllerBase {

  /**
   * The documentation generator chapter manager.
   *
   * @var \Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterManager
   */
  protected $chapterManager;

  /**
   * The renderer for rendering indentation lines.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Build an Overview Controller object.
   *
   * @param \Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterManager $chapterManager
   *   The documentation generator chapter manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer for rendering indentation lines.
   */
  public function __construct(DocumentationGeneratorChapterManager $chapterManager, RendererInterface $renderer) {
    $this->chapterManager = $chapterManager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.documentation_generator_chapter.processor'),
      $container->get('renderer')
    );
  }

  /**
   * Generates the overview form.
   *
   * @return array
   *   Return render array for the overview page.
   */
  public function generateOverview() {
    $definitions = $this->chapterManager->getDefinitions();
    $header = [
      'documentation' => $this->t('Documentation Overview'),
    ];
    $rows = [];
    $row_count = 0;
    foreach ($definitions as $definition) {
      $plugin = $this->chapterManager->createInstance($definition['id']);

      if ($plugin->available()) {
        $elements = $plugin->elements();
        foreach ($elements as $element) {
          if (!empty($element['value'])) {
            $row_count = $row_count + 1;
            if ($element['type'] == 'paragraph' && isset($element['parameters'])) {
              $element['value'] = $this->resolveParameter($element['value'], $element['parameters']);
            }
            $rows[$row_count] = [$element['value']];
            if ($element['level'] - 1 > 0) {
              $indentation = [
                '#theme' => 'indentation_documentation',
                '#size' => $element['level'] - 1,
              ];
              $indentation = $this->renderer->render($indentation);
              $rows[$row_count] = [Markup::create($indentation . $element['value'])];
            }
          }
        }
      }
    }
    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];
  }

  /**
   * Helper function to resolve parameter values.
   *
   * @return string
   *   Return string which can be displayed in documentation overview.
   */
  public function resolveParameter(string $value, ?array $parameters) {
    $computed_value = '';
    $parts = explode('@parameter', $value);
    foreach ($parts as $k => $v) {
      if (!empty($v)) {
        $computed_value .= Html::escape($v);
      }
      if (!empty($parameters[$k])) {
        switch ($parameters[$k]['type']) {
          case 'link':
            $computed_value .= '<a href=' . $parameters[$k]['src'] . '>' . $parameters[$k]['text'] . '</a>';
            break;

          case 'list':
            $computed_value .= '<ul>';
            foreach ($parameters[$k]['items'] as $item) {
              $computed_value .= '<li>' . $item . '</li>';
            }
            $computed_value .= '</ul>';
            break;
        }
      }
    }

    return $computed_value;
  }

}
