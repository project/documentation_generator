<?php

namespace Drupal\documentation_generator\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Documentation Generator Render item annotation object.
 *
 * @see \Drupal\documentation_generator\Plugin\DocumentationGeneratorRenderManager
 * @see plugin_api
 *
 * @Annotation
 */
class DocumentationGeneratorRender extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
