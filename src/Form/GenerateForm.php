<?php

namespace Drupal\documentation_generator\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterManager;
use Drupal\documentation_generator\Plugin\DocumentationGeneratorRenderManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides documentation generation form.
 *
 * @internal
 */
class GenerateForm extends FormBase {

  /**
   * The documentation generator chapter manager.
   *
   * @var \Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterManager
   */
  protected $chapterManager;

  /**
   * The documentation generator render manager.
   *
   * @var \Drupal\documentation_generator\Plugin\DocumentationGeneratorRenderManager
   */
  protected $renderManager;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Build a Generate Form object.
   *
   * @param \Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterManager $chapterManager
   *   The documentation generator chapter manager.
   * @param \Drupal\documentation_generator\Plugin\DocumentationGeneratorRenderManager $renderManager
   *   The documentation generator chapter manager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(DocumentationGeneratorChapterManager $chapterManager,
                              DocumentationGeneratorRenderManager $renderManager,
                              FileSystemInterface $fileSystem,
                              MessengerInterface $messenger,
                              ConfigFactoryInterface $configFactory) {
    $this->chapterManager = $chapterManager;
    $this->renderManager = $renderManager;
    $this->fileSystem = $fileSystem;
    $this->messenger = $messenger;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.documentation_generator_chapter.processor'),
      $container->get('plugin.manager.documentation_generator_render.processor'),
      $container->get('file_system'),
      $container->get('messenger'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'documentation-generator-generate-form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $definitions = $this->renderManager->getDefinitions();

    $options = [];
    foreach ($definitions as $definition) {
      $plugin = $this->renderManager->createInstance($definition['id']);

      $options[$plugin->getPluginId()] = $plugin->getPluginDefinition()['label'];
    }

    if (!empty($options)) {
      $form['render_plugin'] = [
        '#title' => $this->t('Choose the render plugin to use'),
        '#type' => 'radios',
        '#options' => $options,
        '#required' => TRUE,
      ];

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Generate'),
      ];
    }
    else {
      $form['no_plugin'] = [
        '#markup' => $this->t('There is no render plugin available.'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $pluginId = $form_state->getValue('render_plugin');

    $plugin = $this->renderManager->createInstance($pluginId);

    $fileName = 'documentation.' . $plugin->getExtension();
    $privatePath = $this->fileSystem->realpath("private://");
    $title = $this->t('@website Documentation', [
      '@website' => $this->configFactory->get('system.site')->get('name'),
    ])->render();
    $groups = [];

    $definitions = $this->chapterManager->getDefinitions();
    foreach ($definitions as $definition) {
      $chapterPlugin = $this->chapterManager->createInstance($definition['id']);

      if ($chapterPlugin->available()) {
        $groups[] = $chapterPlugin->elements();
      }
    }

    if ($privatePath && $plugin->render($fileName, $privatePath, $title, $groups)) {
      $this->messenger->addMessage($this->t('Documentation generation was successfully performed'));

      $this->messenger->addMessage($this->t('You can now download your documentation at : @link', [
        '@link' => Link::fromTextAndUrl($fileName, Url::fromRoute('system.private_file_download', [
          'filepath' => $fileName,
        ]))->toString(),
      ]));
    }
    else {
      $this->messenger->addMessage($this->t('There was a problem while generating the documentation.'), 'error');
      $this->messenger->addMessage($this->t('Please, check the private path configuration at @url and ensure the private directory exists.', [
        '@url' => Link::createFromRoute('/admin/config/media/file-system', 'system.file_system_settings')->toString(),
      ]), 'error');
    }
  }

}
