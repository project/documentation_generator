<?php

namespace Drupal\documentation_generator\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Documentation Generator Plugins.
 *
 * @internal
 */
class SettingsPluginForm extends ConfigFormBase {

  /**
   * The documentation generator chapter manager.
   *
   * @var \Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterManager
   */
  protected $chapterManager;

  /**
   * Constructs a SettingsPluginForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterManager $chapterManager
   *   The documentation generator chapter manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, DocumentationGeneratorChapterManager $chapterManager) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->chapterManager = $chapterManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('plugin.manager.documentation_generator_chapter.processor')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'documentation-generator-settings-plugin-form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['documentation_generator.enabled_plugins'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('documentation_generator.enabled_plugins');
    $definitions = $this->chapterManager->getDefinitions();

    $options = [];
    foreach ($definitions as $definition) {
      $plugin = $this->chapterManager->createInstance($definition['id']);

      $options[$plugin->getPluginId()] = $plugin->getPluginDefinition()['label'];
    }

    $form['enabled_plugins'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Plugins Enabled'),
      '#default_value' => $config->get('plugins'),
      '#options' => $options,
      '#description' => $this->t('Determine the plugins which will appears in the documentation.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('documentation_generator.enabled_plugins')
      ->set('plugins', $form_state->getValue('enabled_plugins'));

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
