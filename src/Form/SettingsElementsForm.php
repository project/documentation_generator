<?php

namespace Drupal\documentation_generator\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Documentation Generator elements.
 *
 * @internal
 */
class SettingsElementsForm extends ConfigFormBase {

  /**
   * The documentation generator chapter manager.
   *
   * @var \Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterManager
   */
  protected $chapterManager;

  /**
   * Constructs a SettingsElementsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterManager $chapterManager
   *   The documentation generator chapter manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              DocumentationGeneratorChapterManager $chapterManager) {
    parent::__construct($config_factory);
    $this->chapterManager = $chapterManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.documentation_generator_chapter.processor')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'documentation-generator-settings-elements-form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['documentation_generator.disabled_elements'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('documentation_generator.disabled_elements');
    $disabledElements = $config->get('elements');

    $plugins = $this->configFactory->get('documentation_generator.enabled_plugins')->get('plugins');

    foreach ($plugins as $plugin) {
      $plugin = $this->chapterManager->createInstance($plugin);
      $pluginId = $plugin->getPluginId();

      $options = [];

      $elements = $plugin->pluginElements();
      foreach (array_keys($elements) as $value) {
        $options[$pluginId . '_' . $value] = $value;
      }

      $form[$pluginId] = [
        '#type' => 'checkboxes',
        '#title' => $plugin->getPluginDefinition()['label'],
        '#default_value' => $disabledElements,
        '#options' => $options,
        '#description' => $this->t('Determine the plugins elements which will NOT appears in the documentation.'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $plugins = $this->configFactory->get('documentation_generator.enabled_plugins')->get('plugins');
    $values = $form_state->getValues();

    $disabledElements = [];
    foreach ($plugins as $plugin) {
      foreach ($values[$plugin] as $value) {
        if (!empty($value)) {
          $disabledElements[] = $value;
        }
      }
    }

    $config = $this->config('documentation_generator.disabled_elements')
      ->set('elements', $disabledElements);

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
