<?php

namespace Drupal\documentation_generator\Plugin\DocumentationGeneratorChapter;

use Drupal\Core\Url;
use Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterBase;

/**
 * Sample plugin to demonstrate how to create a Documentation Generator chapter.
 *
 * @DocumentationGeneratorChapter(
 *   id = "sample_documentation_plugin",
 *   label = @Translation("Sample Documentation Plugin")
 * )
 */
class SampleDocumentationPlugin extends DocumentationGeneratorChapterBase {

  /**
   * {@inheritdoc}
   */
  public function moduleDependencies() {
    // Mention the machine name of module which should installed for
    // documentation chapter to render.
    return [
      'node',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function pluginElements() {
    // Use this function to run the required entity query.
    $nodeTypes = $this->entityTypeManager
      ->getStorage('node_type')
      ->loadMultiple();

    return $nodeTypes;
  }

  /**
   * {@inheritdoc}
   */
  public function elements() {
    // The elements array will contain all the documentation content.
    $elements = [];

    // Use the 'type' key to indicate if element is 'title' or a 'paragraph'.
    // The 'level' key will determine the indentation level of the element.
    $elements[] = [
      'type' => 'title',
      'level' => 1,
      'value' => $this->t('Sample Documentation Title')->render(),
    ];

    $url = Url::fromUserInput('/admin/content')->setAbsolute()->toString();

    // The '@paramter' string can be added to introduce paramters in the element
    // content. It can be of type 'link' with other keys as 'text' and 'src'.
    $elements[] = [
      'type' => 'paragraph',
      'level' => 2,
      'value' => $this->t('This section provides information about Node Types : @parameter')->render(),
      'parameters' => [
        0 => [
          'type' => 'link',
          'text' => $url,
          'src' => $url,
        ],
      ],
    ];

    $items = [
      'List Point 1',
      'List Point 2',
    ];

    // Parameter can also have 'list' with 'items' being an array.
    // The content of items array will be displayed as list items.
    $elements[] = [
      'type' => 'paragraph',
      'level' => 3,
      'value' => $this->t('This menu is displayed : @parameter')->render(),
      'parameters' => [
        0 => [
          'type' => 'list',
          'items' => $items,
        ],
      ],
    ];

    return $elements;
  }

}
