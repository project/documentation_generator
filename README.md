CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Documentation generator module will help you to generate a user guide of your
website. It lists down all the specifications on a single page as well as
provides a mechanism to export the information to word (docx) file. It combines
the information provided by various plugins, so that it is easy for users to
understand the content architecture and important information of the website.
Developers can also create their own plugins based on sample plugins (present
inside sample directory) to further improve the generated documentation.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/documentation_generator

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/documentation_generator


Requirements
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * Go to Administration » Configuration » Content Authoring » Administer your
   user guide to check the overview page of the module.

 * The main overview page will display the generated user guide of the website.

 * Go to 'Generate' tab to export the user guide in the allowed formats.

 * 'Eanbled Plugin' will show the documentation generation plugins in the
    system. The plugins which aren't needed in the documentation can be
    disabled from here.

 * 'Disabled Elements' tab will allow you to have more granular control over
    items which are not needed in the documentation.

 * Configure the user permissions in Administration » People » Permissions:

   - administer documentation generator

     Users with this permission will be able to access to the administration
     page of Documentation Generator.

MAINTAINERS
-----------

Current maintainers:
 * Léo Prada  (Nixou) - https://www.drupal.org/u/nixou
 * Gaurav Kapoor (gaurav.kapoor) - https://www.drupal.org/u/gauravkapoor
