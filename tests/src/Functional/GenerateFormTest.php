<?php

namespace Drupal\Tests\documentation_generator\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * @coversDefaultClass \Drupal\documentation_generator\Form\GenerateForm
 * @group documentation_generator
 */
class GenerateFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'documentation_generator',
  ];

  /**
   * Tests an anonymous user attempting to access the Generate Form page.
   */
  public function testGeneratePageViewAsAnonymous() {
    $this->drupalGet('admin/config/documentation-generator/generate');
    $this->assertSession()->statusCodeEquals(403);
  }

}
