<?php

namespace Drupal\Tests\documentation_generator\Unit\Controller;

use Drupal\documentation_generator\Controller\OverviewController;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\documentation_generator\Controller/OverviewViewController
 * @group documentation_generator
 */
class OverviewControllerTest extends UnitTestCase {

  /**
   * Tests the resolveParameters method.
   */
  public function testModuleDependencies() {
    $documentationGeneratorChapterManager = $this->createMock('Drupal\documentation_generator\Plugin\DocumentationGeneratorChapterManager');
    $renderer = $this->createMock('Drupal\Core\Render\RendererInterface');
    $overviewController = new OverviewController($documentationGeneratorChapterManager, $renderer);
    $output = $overviewController->resolveParameter('This section provides information about Node Types : @parameter', [
      0 => [
        'type' => 'link',
        'text' => '/admin/content',
        'src' => '/admin/content',
      ],
    ]);
    $this->assertEquals('This section provides information about Node Types : <a href=/admin/content>/admin/content</a>', $output);
    $items = [
      'Menu Item 1',
      'Menu Item 2',
    ];
    $output = $overviewController->resolveParameter('This menu is displayed : @parameter', [
      0 => [
        'type' => 'list',
        'items' => $items,
      ],
    ]);
    $this->assertEquals('This menu is displayed : <ul><li>Menu Item 1</li><li>Menu Item 2</li></ul>', $output);

  }

}
