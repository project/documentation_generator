<?php

namespace Drupal\Tests\documentation_generator\Unit\Plugin\DocumentationGeneratorChapter;

use Drupal\documentation_generator\Plugin\DocumentationGeneratorChapter\BlockContentType;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\documentation_generator\Plugin\DocumentationGeneratorChapter\BlockContentType
 * @group documentation_generator
 */
class BlockContentTypeTest extends UnitTestCase {

  /**
   * Tests the ModuleDependencies method.
   */
  public function testModuleDependencies() {
    $entityTypeManager = $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface');
    $entityFieldManager = $this->createMock('Drupal\Core\Entity\EntityFieldManagerInterface');
    $moduleHandler = $this->createMock('Drupal\Core\Extension\ModuleHandlerInterface');
    $configFactory = $this->createMock('Drupal\Core\Config\ConfigFactoryInterface');

    $blockContentType = new BlockContentType([], '', [], $entityTypeManager, $entityFieldManager, $moduleHandler, $configFactory);

    $dependencies = $blockContentType->moduleDependencies();

    $this->assertIsArray($dependencies);
  }

}
